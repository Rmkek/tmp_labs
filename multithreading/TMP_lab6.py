from datetime import datetime
import random
import string
from multiprocessing.dummy import Pool as ThreadPool


def write_to_file(x):
    with open(f"{x}_output.txt", "w") as file:
        for i in range(100000):
            file.write(
                f"{i} - {''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase, k=m))}\n"
            )


def read_from_write_to(x):
    with open(f"{x}_output.txt", "r") as file_r, open(
        f"final_output.txt", "a"
    ) as file_w:
        file_w.write("".join(file_r.readlines()))


def calculate_timings(func, n):
    st = datetime.now()
    pool.map(func, [i for i in range(1, n + 1)])
    return (datetime.now() - st).total_seconds()


n = int(input("Amount of files: "))
m = int(input("Amount of random letters: "))
pool = ThreadPool(n)


print(
    f"Threadpool filewrite finished in {calculate_timings(write_to_file, n)} seconds\n"
)
print(
    f"Threadpool finished reading ({n}) files in one in {calculate_timings(read_from_write_to, n)} seconds\n"
)
