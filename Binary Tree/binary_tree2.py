class Node(object):
    def __init__(self, value=None, left=None, right=None, offset=None):
        self.value = value
        self.left = left
        self.right = right
        self.offset = offset  # номер строки

    def __str__(self):
        return str(self.value)


class Tree:
    def __init__(self):
        self.root = None

    def add(self, val, off):
        if not self.root:
            self.root = Node(val)
            self.offset = off
        else:
            self.__add(val, self.root, off)

    def __add(self, val, node, off):
        if val < node.value:
            if node.left:
                self.__add(val, node.left, off)
            else:
                node.left = Node(val, offset=off)
        else:
            if node.right:
                self.__add(val, node.right, off)
            else:
                node.right = Node(val, offset=off)

    def search(self, root, key):
        if not root or root.value == key:
            return f"Hash {root } found at line {root.offset}"

        if root.value < key:
            return self.search(root.right, key)
        return self.search(root.left, key)
