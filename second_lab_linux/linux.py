import time
import os
from enum import Enum

BYTES_READ = 100_000_000


class Color(Enum):
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"


def create_file(filename, data):
    fd = os.open(filename, os.O_RDWR | os.O_CREAT)

    try:
        os.write(fd, bytes(data, "UTF-8"))
    finally:
        os.close(fd)


def read_file(filename):
    fd = os.open(filename, os.O_RDONLY)

    data = None

    try:
        data = os.read(fd, BYTES_READ).decode("utf-8")
        return data
    finally:
        os.close(fd)


def get_line(filename, line):
    fd = os.open(filename, os.O_RDONLY)

    data = None

    try:
        data = os.read(fd, BYTES_READ).decode("utf-8")
        data = data.split("\n")[line]
        return data
    finally:
        os.close(fd)


def print_colorful(data, color: Color):
    return f"{color}{data}{Color.ENDC.value}"


create_file("test.txt", "Hello, world!")
read_file("test.txt")


n = 1_000_000


start_1w = time.process_time()
numbers = [str(i) for i in range(n)]
data_to_write = "\n".join(numbers)
with open("strings.txt", "w") as f:
    f.write(data_to_write)
print(f"Write to file using standard lib: {time.process_time() - start_1w}")

start_1r = time.process_time()
f = open("strings.txt").readlines()
print(f"Read file using standard lib: {time.process_time() - start_1r}")

start_2w = time.process_time()
create_file("strings2.txt", data_to_write)
print(f"Write to file using os: {time.process_time() - start_2w}")

start_2r = time.process_time()
data = read_file("strings2.txt")
print(f"Read file using os: {time.process_time() - start_2r}")

print(get_line("strings2.txt", 322))
print(print_colorful(get_line("strings2.txt", 322), Color.OKCYAN.value))
