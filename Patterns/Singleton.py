class SingletonMeta(type):

    # all instances are stored in hashmap here
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class Singleton(metaclass=SingletonMeta):
    def print_self(self):
        print(f"I am - {self}")


if __name__ == "__main__":
    something1 = Singleton()
    something2 = Singleton("test")

    something1.print_self()
    something2.print_self()
    print(something1 == something2)
