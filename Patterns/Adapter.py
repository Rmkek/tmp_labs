class Target:
    def request(self):
        return "Target__request"


class Adaptee:
    def specific_request(self):
        return "Not finished request. Needs adaptation."


class Adapter(Target, Adaptee):
    def request(self):
        adaptee_temp = self.specific_request()
        return f"Adapted adaptee: {adaptee_temp[22:]}"


def main_code(target: "Target"):
    print(target.request())


if __name__ == "__main__":

    target = Target()
    adaptee = Adaptee()
    adapter = Adapter()

    main_code(target)
    print(f"Adaptee: {adaptee.specific_request()}")
    main_code(adapter)
